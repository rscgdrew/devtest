# Web Exercise #

1. Clone the project repository with the ssh url: git@bitbucket.org:rscgdrew/devtest.git
2. Create a new branch for your Web Exercise called Firstname-Lastname (using your first and last name)
3. Complete the Web Exercise Task
4. Push your branch to a remote branch of the same name for review.


## Task Description: ##

Using the design resource in the repository [Web-Exercise.zip], your task is to code the blog layout in a new WordPress Theme. Posts have already been added to the WordPress database and an .sql file can be found in the project root for your use although the database has already been set up for you. You can use Sequel Pro to modify the database as you need. You can login to wordpress admin if needed with:

Username: developer
Password: devtest

You have no more than 3 hours to complete this task. 

To keep the exercise consistent and fair for all applicants, verbal instructions or questions will not be allowed. If you have a technical problem with the computer, programs, or server please contact management.  

Please read the exercise instructions thoroughly and complete the exercise within 3 hours.

### Requirements: ###

**Blog Layout**

* Blog Posts should pull from WordPress Database (posts have already been added and assigned to a category). 
* Navigation links with roll-over state
* Sidebar Widgets (Note: Social Media counts do not need to be dynamic)

**Technical Specifications:**

Although the site is not expected to perform well via organic traffic, standard search engine optimization techniques should be utilized as much as possible while carefully balancing the time given to complete the website exercise. A list of technical recommendations are listed below but primary focus should be on completing the exercise in the allotted time so choose techniques carefully.

* Table-less design (CSS)
* Should be designed for possible expansion in the future (adding more sections), and additions should be easy
* Web standards (W3C) and accessibility standards
* Exciting and creative interactivity
* Optimized for quicker load times

**Technical Requirements (Not limitations):**

*Must use* 

* Must have at least a header and footer in theme files
* Links must have rollover state
* Posts must have a Featured Image


**Tips and Rules:**

* You are welcome to use any scripts such as javascript or php
* You are free to use the Adobe Creative Suite and the resources that accompany them to design or develop any portion of the website task